FROM hseeberger/scala-sbt:16.0.2_1.5.5_

RUN apt update
RUN apt install rsync -y
RUN apt install npm -y
RUN npm install jsdom
